<?php
    namespace App\Controllers;  

    use CodeIgniter\Controller;
    use App\Models\Datamodel;

    class Mahasiswa extends Controller
    {
        function __construct()
        {
            $this->data = new Datamodel();
        }

        function index()
        {
            $data['mhs'] = $this->data->getData();
           return view("mahasiswa",$data);
        }

        function edit()
        {
            $data = $this->data->getDataBarang();
            foreach($data->getResult() as $dtbrg){
                echo $dtbrg->kd_brg, "<br>";
                echo $dtbrg->nm_brg, "<br>";
                echo $dtbrg->harga, "<br>";
            }
        }
        
    }